//変数宣言
//Pointer使用
var Pointer = require('Pointer');
//GameScreen使用
var GameScreen = require('GameScreen');

cc.Class({
    extends: cc.Component,

    properties: {
        //Pointer
        pointer: {
            default: null,
            type: Pointer
        },
        //Arrow
        arrow: {
            default: null,
            type: cc.Node
        },
        //GameScreen
        gameScreen: {
            default: null,
            type: GameScreen
        },

        //Arrowのスケール保存用
        maxScale: 0,
        dragSensibility: 0.1
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        //Arrowの設定されているスケールを最大値として保存
        this.maxScale = this.arrow.scale;
        //Arrowのスケールを0に設定
        this.arrow.scale = 0;
        this.timer = 0;
    },

    update: function (dt) {
        //今玉を発射しているならUpdataしない
        if (this.gameScreen.returnIsShotJudge() == true) return;
        //今タッチしているか
        if (this.pointer.returnIsTouching() == true)
        {
            var dragDistance = this.pointer.distanceBetweenStartTouchPointAndCurrentTouchPoint();
            var isTouchingDown = this.pointer.isTouchingDown();
            if (dragDistance > this.dragSensibility && isTouchingDown)
            {
                if (this.arrow.scale <= this.maxScale)
                {
                    this.arrow.scale += 0.05;
                }
            }
            else
            {
                if (this.arrow.scale >= 0)
                {
                    this.arrow.scale -= 0.05;
                }
            }
        }
        else
        {
            if (this.arrow.scale > 0)
            {
                //玉を撃つ
                this.gameScreen.shootTheBall(this.pointer.returnLastRotation());
            }
            this.arrow.scale = cc.p(0, 0);
        }
    }
});
