cc.Class({
    extends: cc.Component,

    properties: {
        _preLoadedAdRewardedVideo: null,
        _preLoadedAdInterstitial: null,
        //リワード広告ID
        adRewardedVideoID: '',
        //インタースティシャル広告ID
        adInterstitialID: '',
        //インタースティシャル広告表示確率
        _adInterstitialDisplayProbability: null,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        //乱数セット(0〜10)
        this._adInterstitialDisplayProbability = Math.floor( Math.random () * 10) + 1;
        console.log('_adInterstitialDisplayProbability:' + this._adInterstitialDisplayProbability);

        this.adRewardedLoad();

        //40％の確率でインタースティシャル広告読み込み
        if (this._adInterstitialDisplayProbability < 5)
        {
            this.adInterstitialLoad();
        }
    },

    //リワード広告の読み込み
    adRewardedLoad: function () {
        const self = this;
        if (typeof FBInstant == 'undefined' || self._preLoadedAdRewardedVideo != null) return;
        console.log('リワード読み込み開始');
        self._preLoadedAdRewardedVideo = null;
        FBInstant.getRewardedVideoAsync(self.adRewardedVideoID).then(function(rewarded) {
                console.log('リワード読み込み');
                self._preLoadedAdRewardedVideo = rewarded;
                console.log(self._preLoadedAdRewardedVideo);
                return self._preLoadedAdRewardedVideo.loadAsync();
            }).then(function() {
                console.log('リワード読み込み完了')
            }).catch(function(error){
                console.error('リワードの読み込みに失敗しました：' + error.message);
        });
    },

    //インタースティシャル広告読み込み
    adInterstitialLoad: function () {
        const self = this;
        if (typeof FBInstant == 'undefined' || self._preLoadedAdInterstitial != null) return;
        console.log('インタースティシャル読み込み開始');
        self._preLoadedAdInterstitial = null;
        FBInstant.getInterstitialAdAsync(self.adInterstitialID).then(function(interstitial) {
                console.log('インタースティシャル読み込み');
                self._preLoadedAdInterstitial = interstitial;
                console.log(self._preLoadedAdInterstitial);
                return self._preLoadedAdInterstitial.loadAsync();
            }).then(function () {
                console.log('広告を読み込み')
            }).catch(function (error) {
                console.error('広告の読み込みに失敗しました：' + error.message);
        });
    },

    //広告動画再生
    showRewardedAd: function() {
        const self = this;
        if (self._preLoadedAdRewardedVideo == null) return;
        //正常に動画を読み込んでいると再生する
        self._preLoadedAdRewardedVideo.showAsync().then(function() {
            console.log('リワード動画再生');
            self._preLoadedAdRewardedVideo = null;
        }).catch(function(error) {
            console.error(error.message);
        });
    },

    //インタースティシャル広告表示
    showInterstitialAd: function () {
        const self = this;
        if (self._preLoadedAdInterstitial == null) return;
        //正常に広告を読み込んでいると表示
        self._preLoadedAdInterstitial.showAsync().then(function () {
            console.log('インタースティシャル広告表示');
            self._preLoadedAdInterstitial = null;
        }).catch(function (error) {
            console.log(error.message);
        });
    }
});
