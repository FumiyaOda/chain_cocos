cc.Class({
    extends: cc.Component,

    properties: {
        //ブロック
        block: {
            default: null,
            type: cc.Prefab
        },
        //コイン
        coin: {
            default: null,
            type: cc.Prefab
        },
        //ボール加算ポイント
        ballAddPoint: {
            default: null,
            type: cc.Prefab
        },
        
        firstObjectSetPositionX: -300,
        addObjectPositionX: 100,

        _gameScreenNode: null,
        _gameScreen: null,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        //オブジェクト生成する
        this.objectCreate();
    },

    update: function (dt) {
        //子のノードが全て無くなると自身を削除
        if (this.node.childrenCount <= 0)
        {
            this.node.destroy();
        }
    },

    //オブジェクト生成
    objectCreate: function () {
        for(let turn = 0; turn < 6; turn += 1|0)
        {
            const random = Math.floor( Math.random () * 10) + 1;
            switch(random)
            {
                //ブロック生成
                case 1:
                case 2:
                case 3:
                case 4:
                const blockInstans = cc.instantiate(this.block);
                blockInstans.setPosition(cc.v2(this.firstObjectSetPositionX + this.addObjectPositionX * turn, 0));
                this.node.addChild(blockInstans, 2, 0);
                break;

                //ボール加算ポイント生成
                case 5:
                case 6:
                const ballAddPointInstans = cc.instantiate(this.ballAddPoint);
                ballAddPointInstans.setPosition(cc.v2(this.firstObjectSetPositionX + this.addObjectPositionX * turn, 0));
                this.node.addChild(ballAddPointInstans, 2, 0);
                break;

                //何も生成しない
                case 7:
                case 8:
                case 9:
                break;

                //コイン生成
                case 10:
                const coinInstans = cc.instantiate(this.coin);
                coinInstans.setPosition(cc.v2(this.firstObjectSetPositionX + this.addObjectPositionX * turn, 0));
                this.node.addChild(coinInstans, 2,0);
                break;
            }
        }
    },
});
