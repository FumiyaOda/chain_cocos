//ショップ機能全般

//変数宣言
const DataManager = require("DataManager");
const ShopItem = require("ShopItem");
const TitleScreen = require("TitleScreen");

cc.Class({
    extends: cc.Component,

    properties: {
        //DataManager
        dataManager: {
            default: null,
            type: DataManager
        },
        //titleScreen
        title: {
            default: null,
            type: TitleScreen
        },
        //タイトル画面
        titleScreen: {
            default: null,
            type: cc.Node
        },
        //ショップ画面
        shopScreen: {
            default: null,
            type: cc.Node
        },
        //ショップ商品
        shopItems: {
            default: [],
            type: ShopItem
        },
        //商品名ラベル
        shpoItemLabel: {
            default: null,
            type: cc.Label
        },
        //コインアイコン
        coinIcon: {
            default: null,
            type: cc.Node
        },
        //所持コイン表示ラベル
        coinPointsLabel: {
            default: null,
            type: cc.Label
        },
        //商品ノード
        shopBallsContainer: {
            default: null,
            type: cc.Node
        },
        //ヒントラベル
        selectedItemPriceLabel: {
            default: null,
            type: cc.Node
        },
        //プレイボタン
        playButton: {
            default: null,
            type: cc.Node
        },
        //アンロックボタン
        unLockedButton: {
            default: null,
            type: cc.Node
        },

        //Canvas
        canvas: cc.Node,
        //通常の大きさの拡大率
        _normalScale: 0,
        //縮小した大きさの拡大率
        _tinyScale: 0,
        //スワイプ用変数
        _previousTouchPosition: 0,
        //スワイプ用変数
        _positionVariation: 0,
        //タッチした座標保存用変数
        _touchStart: 0,
        //スワイプ時にタッチしている座標
        _touchMove: 0,
        //商品配列のポインタ
        _pointer: 0
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        //それぞれの拡大率を設定
        this._normalScale = cc.p(1, 1);
        this._tinyScale = cc.p(0.3, 0.3);
        //配列に入っている商品名を表示
        this.shpoItemLabel.string = this.shopItems[this._pointer].ball.name;
        //予め拡大しておく
        this.shopItems[this._pointer].ball.scale = this._normalScale;

        var self = this;
        self.isMoving = false;

        //タッチ開始時処理
        self.canvas.on(cc.Node.EventType.TOUCH_START, function(event) {
            self.isMoving = true;
            var touches = event.getTouches();
            self._touchStart = touches[0].getLocationX();
        }, self.node);
        //スワイプ処理
        self.canvas.on(cc.Node.EventType.TOUCH_MOVE, function(event) {
            var touches = event.getTouches();
            self._touchMove = touches[0].getLocationX();
            self._touchStart = self._touchMove;

            var newItemContainerPosition = self.shopBallsContainer;
            newItemContainerPosition.x += self._positionVariation;
            self.shopBallsContainer = newItemContainerPosition;
        }, self.node);
        //タッチ終了処理
        self.canvas.on(cc.Node.EventType.TOUCH_END, function (event) {
            self.isMoving = false;
            console.log("Touch_End");
        }, self.node);
        //タッチキャンセル処理
        self.canvas.on(cc.Node.EventType.TOUCH_CANCEL, function (event) {
            self.isMoving = false;
        }, self.node);
    },

    start: function () {
        //現在所持しているコインの枚数を表示
        this.coinPointsLabel.string = this.dataManager.returnPossessedCoin() + "/9999";
        //商品のアンロック状態でGUIを切り替え
        this.shopGUISwitching(this.shopItems[0].isUnlocked);
    },

    update: function (dt) {
        //タッチしていない
        if (!this.isMoving)
        {
            this._previousTouchPosition = this._touchStart;

            if (this.shopBallsContainer.x > -80)
            {
                this.shopBallsContainer.x = 0;
                this._pointer = 0;
                this.shopItemsGUISwitiching();
                //console.log("しんぷるぼーる");
            }
            else if (this.shopBallsContainer.x < -81 && this.shopBallsContainer.x > -255)
            {
                this.shopBallsContainer.x = -175;
                this._pointer = 1;
                this.shopItemsGUISwitiching();
                //console.log("こいんぼーる");
            }
            else {
                this.shopBallsContainer.x = -350;
                this._pointer = 2;
                this.shopItemsGUISwitiching();
                //console.log("ぎがんとぼーる");
            }
            /*else if (this.shopBallsContainer.x < -256 && this.shopBallsContainer.x > -425)
            {
                this.shopBallsContainer.x = -350;
                this._pointer = 2;
                this.shopItemsGUISwitiching();
                //console.log("ぎがんとぼーる");
            }
            else
            {
                this.shopBallsContainer.x = -525;
                this._pointer = 3;
                this.shopItemsGUISwitiching();
                //console.log("たいにーぼーる");
            }*/
        }
        //タッチしている
        else
        {
            //ボールのスケールを小さく
            this.shopItems[this._pointer].ball.scale = this._tinyScale;
            this._positionVariation = this._touchStart - this._previousTouchPosition;
            this._previousTouchPosition = this._touchStart;
        }        
    },

    //商品UI表示切り替え
    shopItemsGUISwitiching: function () {
        //選択しているボールを拡大し
        this.shopItems[this._pointer].ball.scale = this._normalScale;
        //選択しているボールの名前を表示
        this.shpoItemLabel.string = this.shopItems[this._pointer].ball.name;
        //選択しているボールのロック状態を確認してUIの表示を切り替える
        this.shopGUISwitching(this.shopItems[this._pointer].isUnlocked);
    },

    //ShopGUI項目の表示切り替え
    shopGUISwitching: function (bool) {
        //ボールのロック状態を引っ張ってきてUIの表示を切り替える
        if (bool)
        {
            //コインのアイコン
            this.coinIcon.active = false;
            //所持コインテキスト
            this.coinPointsLabel.string = "";
            //ヒントテキスト
            this.selectedItemPriceLabel.active = false;
            //プレイボタン
            this.playButton.active = true;
            //ロック解除ボタン
            this.unLockedButton.active = false;
        }
        else
        {
            this.coinIcon.active = true;
            this.coinPointsLabel.string = this.dataManager.returnPossessedCoin() + "/9999";
            this.selectedItemPriceLabel.active = true;
            this.playButton.active = false;
            this.unLockedButton.active = true;
        }
    },

    //タッチイベントの削除
    touchEventDelete: function () {
        console.log("TouchEventDelete");
        this.canvas.targetOff(this);
    },

    //プレイボタンを押した処理
    isPlayButton: function () {
        this.title.getBallName(this.shopItems[this._pointer].ball.name);
        //DataManagerで管理しているフラグを切替
        this.dataManager.switchingBallUnLock(this.shopItems[this._pointer].ball.name, this.shopItems[this._pointer].isUnlocked);
        console.log(this.dataManager.returnBallUnLock(this.shopItems[this._pointer].ball.name));
        //ショップ画面を非表示に
        this.shopScreen.active = false;
        //タイトル画面を表示する
        this.titleScreen.active = true;
        this.touchEventDelete();
    },

    //アンロックボタンを押した処理
    isUnlockedButton: function () {
        console.log("PussUnlokedButton");
        //コインの枚数で条件分岐
        if (this.dataManager.returnPossessedCoin() >= 100)
        {
            console.log("BallGet");
            this.dataManager.consumeCoin(100);
            //選択しているボールのロック状態を解除
            this.shopItems[this._pointer].isUnlocked = true;
            //選択しているボールのロック状態を確認してUIの表示を切り替える
            this.shopGUISwitching(this.shopItems[this._pointer].isUnlocked);
        }
        else
        {
            return;
        }
    }
});
