cc.Class({
    extends: cc.Component,

    properties: {
        _lifeTime: 0.5,
        _moveSpeed: 75,
    },

    // LIFE-CYCLE CALLBACKS:

    update (dt) {
        this._lifeTime -= dt;
        let moveY = this._moveSpeed * dt;
        this.node.y = this.node.y + moveY;
        if (this._lifeTime <= 0)
        {
            this.node.destroy();
        }
    },
});