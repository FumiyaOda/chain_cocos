cc.Class({
    extends: cc.Component,

    properties: {
        gameTitle: "ENERGY CHAIN",
    },

    //招待を送る相手を選択してメッセージを送る
    sendRequest() {
        if (typeof FBInstant == 'undefined') return;
        const self = this;
        FBInstant.context.chooseAsync().then(function() {
            console.log(FBInstant.context.getID());
            FBInstant.updateAsync({
                action: 'CUSTOM',
                cta: 'Play',
                image: self.getImgBase64(),
                text: {
                    default: "An invitation letter for ENERGYCHAIN has arrived from " + FBInstant.player.getName() + "!",
                    localizations: {
                        /*必須言語*/
                        //英語(アメリカ)
                        en_US: "An invitation letter for ENERGYCHAIN has arrived from " + FBInstant.player.getName() + "!",
                        //ポルトガル語(ブラジル)
                        pt_BR: "Uma carta de convite para ENERGYCHAIN chegou de " + FBInstant.player.getName() + "!",
                        //インドネシア語)(インドネシア)
                        id_ID: "Surat undangan untuk ENERGYCHAIN telah tiba dari "　+ FBInstant.player.getName() + "!",
                        //フランス語(フランス)
                        fr_FR: "Une lettre d'invitation pour ENERGY CHAIN est arrivée de " + FBInstant.player.getName() + "!",
                        //ベトナム語(ベトナム)
                        vi_VN: "Một lá thư mời cho NĂNG LƯỢNG CHAIN đã đến từ " + FBInstant.player.getName() + "!",
                        //タイ語(タイ)
                        th_TH: "จดหมายเชิญสำหรับ ENERGY CHAIN มาถึงแล้วจาก " +　FBInstant.player.getName() + "!",
                        //トルコ語(トルコ)
                        tr_TR: "Ben " + FBInstant.player.getName() + " bir ENERGYCHAIN davet aldı!",
                        //ドイツ語(ドイツ)
                        de_DE: "Ein Einladungsschreiben für ENERGYCHAIN ist von " + FBInstant.player.getName() + " eingetroffen!",
                        //スペイン語(スペイン)
                        es_ES: "¡Una carta de invitación para ENERGYCHAIN ha llegado de " + FBInstant.player.getName() + "!",
                        //アラビア語(アメリカ)
                        ar_AE: "تم إرسال خطاب دعوة لـ ENERGYCHAIN من " + FBInstant.player.getName() + "!",
                        
                        /*推奨言語*/
                        //日本語(日本)
                        ja_JP: FBInstant.player.getName() + "さんからENERGYCHAINの招待状が届きました!",
                        //イタリア語(イタリア)
                        it_IT: "Una lettera di invito per ENERGYCHAIN è arrivata da " + FBInstant.player.getName() + "!",
                        //中国語(簡体)
                        zh_Hans: "邀请ENERGYCHAIN从" + FBInstant.player.getName() + "到来了！",
                        //中国語(繁体)
                        zh_Hant: "邀請ENERGYCHAIN從" + FBInstant.player.getName() + "到來了！",
                        //ロシア語(ロシア)
                        ru_RU: "ENERGYCHAIN приглашения прибыл из " + FBInstant.player.getName() + "!",
                        //ポーランド語(ポーランド)
                        pl_PL: "ENERGYCHAIN zaproszenia przybył z " + FBInstant.player.getName() + "!",
                        //オランダ語(オランダ)
                        nl_NL: "Een uitnodigingsbrief voor ENERGYCHAIN is aangekomen van " + FBInstant.player.getName() + "!",
                        //スウェーデン語(スウェーデン)
                        sv_SE: "En inbjudan till ENERGYCHAIN har kommit från " + FBInstant.player.getName() + "!",
                        //スウェーデン語(フィンランド)
                        sv_FI: "ENERGY CHAINin kutsu on saapunut " + FBInstant.player.getName() + "ilta!",
                        //ハンガリー語(ハンガリー)
                        hu_HU: "Meghívást kapott az ENERGY CHAIN-tól a " + FBInstant.player.getName() + "-től!",
                        //ギリシャ語(ギリシャ)
                        el_GR: "Λάβαμε μια πρόσκληση από την ENERGY CHAIN από " + FBInstant.player.getName() + "!",
                        //チェコ語(チェコ)
                        cs_CZ: "Dostali jsme pozvání od ENERGY CHAIN od " + FBInstant.player.getName() + "!",
                    }
                },
                template: 'play_turn',
                data: { 
                    sendRequestID: FBInstant.context.getID(),
                },
                strategy: 'IMMEDIATE',
                notification: 'NO_PUSH',
              }).then(function() {
                console.log('Message was sent successfully');
            }).catch(function () {
                console.log('failed!');
            });
          }).catch(function (error) {
            console.log(error);
        });
    },

    //シェア機能。現状はタイムライン投稿しようとするとtextメッセージが消えてしまう。
    shareGame() {

        if (typeof FBInstant == 'undefined')
            return;

        FBInstant.shareAsync({
            intent: 'SHARE',    //何をするのか
            image: this.getImgBase64(),
            text: FBInstant.player.getName()+' seems to be playing ENRGYCHAIN',     //送るメッセージ
            data: { myReplayData: '...' },
        }).then(() => {
            // continue with the game.
            console.log('共有完了');
        });
    },

    //画面をキャプチャする
    getImgBase64() {
        let target = cc.find('Canvas');
        let width = 720, height = 375;
        let renderTexture = new cc.RenderTexture(width, height);
        renderTexture.begin();
        target._sgNode.visit();
        renderTexture.end();
        let canvas = document.createElement('canvas');      //HTML要素生成。
        let ctx = canvas.getContext('2d');
        canvas.width = width;
        canvas.height = height;                             //縦横幅設定
        if (cc._renderType === cc.game.RENDER_TYPE_CANVAS) {
            let texture = renderTexture.getSprite().getTexture();
            let image = texture.getHtmlElementObj();
            ctx.drawImage(image, width / 2, height / 2);
        }
        //基本的にWebGLらしい。ビルド設定からWebGL優先に設定しているため。
        else if (cc._renderType === cc.game.RENDER_TYPE_WEBGL) {
            let buffer = gl.createFramebuffer();                                                        //フレームバッファ生成
            gl.bindFramebuffer(gl.FRAMEBUFFER, buffer);                                                 //フレームバッファをWebGLにバインド
            let texture = renderTexture.getSprite().getTexture()._glID;
            gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);   //フレームバッファへのテクスチャの紐付け
            let data = new Uint8Array(width * height * 4);
            gl.readPixels(0, 0, width, height, gl.RGBA, gl.UNSIGNED_BYTE, data);                        //←第1,2引数を弄るとオフセットがずれるが、ずれた分は表示されない(真っ白)
            gl.bindFramebuffer(gl.FRAMEBUFFER, null);                                                   //フレームバッファをWebGLにバインド
            let rowBytes = width * 4;
            //プリンタみたいに1行(px)ずつデータ書き込み。
            for (let row = 0; row < height; row++) {
                let srow = height - 1 - row;            //描画する高さ設定。上から順に。
                let data2 = new Uint8ClampedArray(data.buffer, srow * width * 4, rowBytes);
                let imageData = new ImageData(data2, width, 1);     //ピクセルデータ設定。
                ctx.putImageData(imageData, 0, row);            //Canvasに指定のImageDataオブジェクトのデータを描画。
            }
        }
        return canvas.toDataURL('image/png');
    },
});
