//ユーザーのデータ管理全般

//ユーザーデータの定義
var EnergyChainUserData = {
    //最後にプレイしたスコア
    lastScore: 0,
    //今までのベストスコア
    bestScore: 0,
    //所持コイン
    possessedCoin: 0,
    //各ボールのロック状態
    simpleBallUnLock: true,
    coinBallUnLock: false,
    giantBallUnLock: false,
    tinyBallUnLock: false,
    //選択しているボールの名前(予めシンプルボールに設定)
    selectedBall: "Simple Ball",
    //ショートカットを作成したか
    isCreateShortcutFlg: false,
    //初めてゲーム画面に遷移したか
    isFirstPlay: false,
};

cc.Class({
    extends: cc.Component,

    properties: {
        //最後に記録したスコアを表示するラベル(タイトル)
        titleLastScoreLabel: {
            default: null,
            type: cc.Label
        },
        //今までのベストスコアを表示するラベル(タイトル)
        titleBestScoreLabel: {
            default: null,
            type: cc.Label
        },
        resultBestScoreLabel: {
            default: null,
            type: cc.Label
        },
        //Coin所持枚数ラベル(タイトル)
        titleCoinLabel: {
            default: null,
            type: cc.Label
        },
        //Coin所持枚数ラベル(ショップ)
        shopCoinLabel: {
            default: null,
            type: cc.Label
        },
        //Coin所持枚数ラベル(ゲーム)
        gameCoinLabel: {
            default: null,
            type: cc.Label
        },

        //セーブデータとして残すユーザーデータ
        _energyChainUserData: null,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        //データを読み込む
        this.readUserData();

        //各ラベルのテキストを更新
        this.titleLastScoreLabel.string = this._energyChainUserData.lastScore;
        this.titleBestScoreLabel.string = this._energyChainUserData.bestScore;
        this.resultBestScoreLabel.string = this._energyChainUserData.bestScore;
        this.titleCoinLabel.string = this._energyChainUserData.possessedCoin;
        this.shopCoinLabel.string = this._energyChainUserData.possessedCoin + '/9999';
        this.gameCoinLabel.string = this._energyChainUserData.possessedCoin;
    },

    //ユーザーデータ読み込み
    readUserData: function () {
        var data = cc.sys.localStorage.getItem('EnergyChainUserData');
        //確認用ログ
        cc.log(data);

        if (data !== null) {
            //見つかったらセーブファイルのデータを適用
            this._energyChainUserData = JSON.parse(data);
            console.log(this._energyChainUserData.lastScore);
        }
        else {
            //見つからなかったら初期値をセット
            this._energyChainUserData = EnergyChainUserData;
        }
    },

    //ユーザーデータの記録
    writingUseData: function () {
        //JOSNオブジェクトにして保存
        cc.sys.localStorage.setItem('EnergyChainUserData',JSON.stringify(this._energyChainUserData));
    },

    //各ボールのアンロック状態を返す
    returnBallUnLock (ballName) {
        switch (ballName) {
            case "Simple Ball":
            console.log(this._energyChainUserData.simpleBallUnLock);
            return this._energyChainUserData.simpleBallUnLock;

            case "Coin Ball":
            console.log(this._energyChainUserData.coinBallUnLock);
            return this._energyChainUserData.coinBallUnLock;

            case "Giant Ball":
            console.log(this._energyChainUserData.giantBallUnLock);
            return this._energyChainUserData.giantBallUnLock;

            case "Tiny Ball":
            console.log(this._energyChainUserData.tinyBallUnLock);
            return this._energyChainUserData.tinyBallUnLock;
        }
    },

    //各ボールのアンロック状態を変更
    switchingBallUnLock (ballName, unlockBallFlg) {
        console.log(ballName+':'+unlockBallFlg);
        switch (ballName) {
            case "Simple Ball":
            this._energyChainUserData.simpleBallUnLock = unlockBallFlg;
            break;

            case "Coin Ball":
            this._energyChainUserData.coinBallUnLock = unlockBallFlg;
            break;

            case "Giant Ball":
            this._energyChainUserData.giantBallUnLock = unlockBallFlg;
            break;

            case "Tiny Ball":
            this._energyChainUserData.tinyBallUnLock = unlockBallFlg;
            break;
        }
    },

    //ショートカット作成のリワード
    createShortcutReward: function () {
        this._energyChainUserData.possessedCoin += 30|0;
        this._energyChainUserData.isCreateShortcutFlg = true;
        this.writingUseData();
    },

    //How_To画面を閉じたらデータを保存
    isFirstPlaySave: function () {
        this._energyChainUserData.isFirstPlay = true;
        this.writingUseData();
    },

    returnIsFirstPlay: function () {
        return this._energyChainUserData.isFirstPlay;
    },

    returnIsCreateShortcutFlg: function () {
        return this._energyChainUserData.isCreateShortcutFlg;
    },

    returnSelectedBallName: function () {
        return this._energyChainUserData.selectedBall;
    },

    returnPossessedCoin: function () {
        return this._energyChainUserData.possessedCoin;
    },

    //スコア更新
    scoreUpdata: function (score) {
        this._energyChainUserData.lastScore = score;
        if (this._energyChainUserData.bestScore >= score) return;

        this._energyChainUserData.bestScore = score;
        this.resultBestScoreLabel.string = this._energyChainUserData.bestScore;
    },

    //Coinを入手した
    getCoin: function (num) {
        this._energyChainUserData.possessedCoin += num|0;
        this.titleCoinLabel.string = this._energyChainUserData.possessedCoin;
        this.shopCoinLabel.string = this._energyChainUserData.possessedCoin + '/9999';
        this.gameCoinLabel.string = this._energyChainUserData.possessedCoin;

        this.writingUseData();
    },

    //Coinを消費
    consumeCoin: function (num) {
        this._energyChainUserData.possessedCoin -= num|0;
        this.titleCoinLabel.string = this._energyChainUserData.possessedCoin;
        this.shopCoinLabel.string = this._energyChainUserData.possessedCoin + '/9999';
        this.gameCoinLabel.string = this._energyChainUserData.possessedCoin;

        this.writingUseData();
    }
});
