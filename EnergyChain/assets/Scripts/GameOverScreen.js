//変数宣言
const DataManager = require('DataManager');
const TileScreen = require('TitleScreen');
const GameScreen = require('GameScreen');
const Leaderboad = require('Leaderboard');

cc.Class({
    extends: cc.Component,

    properties: {
        dataManager: {
            default: null,
            type: DataManager
        },
        titleScreen: {
            default: null,
            type: TileScreen
        },
        gameScreen: {
            default: null,
            type: GameScreen
        },
        scoreLabel: {
            default: null,
            type: cc.Label
        },
        leaderboad: {
            default: null,
            type: Leaderboad
        },

        leaderboardNode: cc.Node,
        myRankScoreLabel: cc.Label,
        rewardGUINode: cc.Node,
        moveButtonNode: cc.Node,
    },

    onLoad: function () {
        this.scoreLabel.string = this.gameScreen.returnScore();
        this.titleScreen.showInterstitialAd();
        this.myRankScoreLabel.string = this.gameScreen.returnScore();
    },

    isMoveButton: function () {
        this.dataManager.getCoin(10);
        this.rewardGUINode.active = true;
        this.moveButtonNode.active = false;
        this.titleScreen.showRewardedAd();
    },

    isTitleButton: function () {
        this.dataManager.writingUseData();
        //シーンを再読込み
        cc.director.loadScene('Game');
    },

    rankingButton: function () {
        this.leaderboad.sendScore(this.gameScreen.returnScore());
        this.leaderboardNode.active = true;   
    }
});
