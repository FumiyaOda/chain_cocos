cc.Class({
    extends: cc.Component,

    properties: {
        itemObject: {
            default: null,
            type: cc.Node
        },
        audio: {
            default: null,
            url: cc.AudioClip
        },
        _gameScreenNode: null,
        _gameScreen: null,

        _plusLabelCreatorNode: null,
        _plusLabelCreator: null,

        _itemName: ''
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        this.itemName = this.node.name;
        this._gameScreenNode = cc.find('Canvas/GameScreen');
        this._gameScreen = this._gameScreenNode.getComponent('GameScreen');

        this._plusLabelCreatorNode = cc.find('Canvas/GameScreen/PlusLabelCreator');
        this._plusLabelCreator = this._plusLabelCreatorNode.getComponent('PlusLabelCreator');
    },

    onBeginContact: function (contact, self, other)
    {
        if (other.tag == 0)
        {
            const nodePosX = this.node.getPositionX();
            const nodePosY = this.node.getPositionY();

            if (this.itemName == 'PlusBall')
            {
                this._gameScreen.ballPlus();
                this._plusLabelCreator.plusLabelCreator(0, nodePosX, nodePosY + 430);
            }
            else if (this.itemName == 'Coin')
            {
                this._gameScreen.coinPlus();
                this._plusLabelCreator.plusLabelCreator(1, nodePosX, nodePosY + 430);
            }
            cc.audioEngine.playEffect(this.audio, false);
            this.node.destroy();
        }

        if (other.tag == 7)
        {
            console.log('test');
            this.node.destroy();
        }
    }
});
