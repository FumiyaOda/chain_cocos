cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.scaleX = 0;
        this.node.scaleY = 0;

        this.schedule(this.labelAnimation, 0.001, 24, 0);
    },

    labelAnimation () {
        this.node.scaleX = (this.node.scaleX + 0.04);
        this.node.scaleY = (this.node.scaleY + 0.04);
    }
});
