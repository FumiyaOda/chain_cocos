cc.Class({
    extends: cc.Component,

    properties: {
        //最初タッチした座標
        startTouchPoint: 0,
        //現在タッチしている座標
        currentTouchPoint: 0,
        //最後にタッチしていた座標
        lastFrameTouchPoint: 0,

        rotationSensibility: 0.3,
        rotationLimit: 80,
        moveToPointerVelocity: 10,

        //Canvas
        canvas: cc.Node,

        character: cc.Node,

        lastRotation: cc.v2,

        isTouching: false,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        var self = this;

        //タッチ開始時処理
        self.canvas.on(cc.Node.EventType.TOUCH_START, function(event) {
            self.isTouching = true;
            var touches = event.getTouches();
            self.startTouchPoint = touches[0].getLocation();
            self.currentTouchPoint = self.startTouchPoint;
            self.lastFrameTouchPoint = self.startTouchPoint;
        }, self.node);
        //スワイプ処理
        self.canvas.on(cc.Node.EventType.TOUCH_MOVE, function(event) {
            self.isTouching = true;
            var touches = event.getTouches();
            self.lastFrameTouchPoint = self.currentTouchPoint;
            self.currentTouchPoint = touches[0].getLocation();
            
			var dragDistance = self.distanceBetweenLastFrameAndNewFrameMousePositionOnXAxis();
			var angleToAdd = -(dragDistance * self.rotationSensibility);
            var newRotation = self.node.rotation + angleToAdd;

			//ポインタを回転させる
			if (-self.rotationLimit < self.node.rotation < self.rotationLimit)
            {
                self.node.rotation = newRotation;
            }
        }, self.node);
        //タッチ終了処理
        self.canvas.on(cc.Node.EventType.TOUCH_END, function (event) {
            self.touchEvent();
        }, self.node);
        //タッチキャンセル処理
        self.canvas.on(cc.Node.EventType.TOUCH_CANCEL, function (event) {
            self.touchEvent();
        }, self.node);
    },

    update (dt) {
        if (this.isTouching)
        {
            if (this.node.rotation < -this.rotationLimit)
            {
                this.node.rotation = -this.rotationLimit;
            }

            if (this.node.rotation > this.rotationLimit)
            {
                this.node.rotation = this.rotationLimit;
            }

            this.lastRotation = this.node.rotation;
        }
        else
        {
            this.node.rotation = 0;
        }
    },

    defaultPositionSet: function (x, y) {
        this.node.setPosition(x, y);
    },

    //タッチ終了時、キャンセル時の処理
    touchEvent: function () {
        this.isTouching = false;
        this.currentTouchPoint = cc.Vec2.ZERO;
        this.lastFrameTouchPoint = this.currentTouchPoint;
    },

    distanceBetweenStartTouchPointAndCurrentTouchPoint: function () {
        var touchStartPoint = cc.v2(this.startTouchPoint.x, this.startTouchPoint.y);
        var touchCurrentPoint = cc.v2(this.currentTouchPoint.x, this.currentTouchPoint.y);
        //startTouchPointとcurrentTouchPointの間の距離を計算
        var distance = cc.pDistance(touchStartPoint, touchCurrentPoint);
        return distance;
    },

    distanceBetweenLastFrameAndNewFrameMousePositionOnXAxis: function () {
        return this.lastFrameTouchPoint.x - this.currentTouchPoint.x;
    },

    isTouchingDown: function () {
        //最初にタッチしたY座標より現在タッチしているY座標が小さい
        if (this.startTouchPoint.y > this.currentTouchPoint.y)
        {
            return true;
        }
        else 
        {
            return false;
        }
    },

    returnIsTouching: function () {
        return this.isTouching;
    },

    returnLastRotation: function () {
        return this.lastRotation;
    },

    positionSet: function () {
        this.node.setPosition(this.character.getPositionX(), this.node.getPositionY());
    }
});
