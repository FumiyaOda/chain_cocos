//変数宣言
const DataManager = require('DataManager');

cc.Class({
    extends: cc.Component,

    properties: {
        dataManager: {
            default: null,
            type: DataManager
        },
        //ポーズ画面
        pauseScreen: {
            default: null,
            type: cc.Node
        },
    },

    // LIFE-CYCLE CALLBACKS:

    resumeButton ()
    {
        cc.director.resume();
        this.pauseScreen.active = false;
    },

    homebutton ()
    {
        this.dataManager.writingUseData();
        //シーンを再読込み
        cc.director.loadScene('Game');
        cc.director.resume();
    }
});
