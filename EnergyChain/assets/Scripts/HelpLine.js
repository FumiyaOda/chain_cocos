//�ϐ��錾
//Pointer�g�p
var Pointer = require('Pointer');

cc.Class({
	extends: cc.Component,

	properties: {
		//Pointer
		pointer: {
			default: null,
			type: Pointer
		},
		//HelpLine
		helpLine: {
			default: null,
			type: cc.Node
		},

		helpLineBaseColor: cc.Color,
		helpLineHiddenAlphaColor: cc.Color,
		pointerScaleRatio: 0.1
	},

	// LIFE-CYCLE CALLBACKS:

	onLoad: function () {
		this.helpLineBaseColor = this.helpLine.color;
		this.helpLineHiddenAlphaColor = this.helpLineBaseColor;
	}
});
