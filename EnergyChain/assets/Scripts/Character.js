cc.Class({
    extends: cc.Component,

    properties: {
		//Pointer
		pointer: {
			default: null,
			type: cc.Node
        }
    },

    // LIFE-CYCLE CALLBACKS:

    defaultPositionSet: function (x, y) {
        this.node.setPosition(x, y);
        if (this.node.getPositionX() <= -340)
        {
            this.node.setPosition(-340, this.node.getPositionY());
        }

        if (this.node.getPositionX() >= 340)
        {
            this.node.setPosition(340, this.node.getPositionY());
        }
    },

    positionSet: function (x) {
        try {
            this.node.setPosition(x, this.node.getPositionY());

            if (this.node.getPositionX() <= -340)
            {
                this.node.setPosition(-340, this.node.getPositionY());
            }

            if (this.node.getPositionX() >= 340)
            {
                this.node.setPosition(340, this.node.getPositionY());
            }
        }
        catch(error) {
            console.log(error);
            return;
        }
    },

    switchingMainBallActive: function (flg) {
        this.node.active = flg;
    }
});
