//変数宣言
const DataManager = require('DataManager');
const TitleScreen = require('TitleScreen');
const Character = require('Character');
const Pointer = require('Pointer');
const SampleBallSprite = require('SampleBallSprite');

cc.Class({
    extends: cc.Component,

    properties: {
        //dataManager
        dataManager: {
            default: null,
            type: DataManager
        },
        //Titlescreen
        titlescreen: {
            default: null,
            type: TitleScreen
        },
        //Character
        character: {
            default: null,
            type: Character
        },
        //Pointer
        pointer: {
            default: null,
            type: Pointer
        },
        sampleBallSprite: {
            default: null,
            type: SampleBallSprite
        },
        //ゲーム画面
        gameScreen: {
            default: null,
            type: cc.Node
        },
        //ゲームオーバー画面
        gameOverScreen: {
            default: null,
            type: cc.Node
        },
        //ポーズ画面
        pauseScreen: {
            default: null,
            type: cc.Node
        },
        //BallsContainer
        ballsContainer: {
            default: null,
            type: cc.Node
        },
        //PointerNode
        pointerNode: {
            default: null,
            type: cc.Node
        },
        //RowPrefab
        rowPrefab: {
            default: null,
            type: cc.Prefab
        },
        //玉プレハブ
        ballPrefab: {
            default: [],
            type: [cc.Prefab]
        },
        _rowArray: {
            default: [],
            type: cc.Node
        },
        //残玉数表示ラベル
        availableBallsLabel: {
            default: null,
            type: cc.Label
        },
        //スコア表示ラベル
        scoreLabel: {
            default: null,
            type: cc.Label
        },
        gameOverAudio: {
            default: null,
            url: cc.AudioClip
        },
        moveAvailableBallsLabel: cc.Node,

        _rowArrayPointer: 0,
        //現在のスコア
        _score: 1,
        //生成するボールの数
        _ballGenerateNum: 1,
        //生成するボールのプレハブを格納した配列を指すポインタ
        _ballCreatePointer: 0,
        //ボール生成に設定する角度
        _ballCreateConfigurationAngre: 0,
        //生成するボールの数を一時的に保存
        _temporaryBallGenerateNum: 0,

        _particleProductionTimer: 1,

        _isGameOver: false,
        _isShoting: false,
        _isShotJudge: false,

        _rowArrayLength: 0,
        _rowArrayChildrenLength: 0,
        _rowMoveCount: 0,

        howToNode: cc.Node,
        _isSpeedUp: false,
        speedButtonLabel: cc.Label,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        if (this.dataManager.returnIsFirstPlay() == true)
        {
            console.log("HowTocloos");
            this.howToCloosButton();
        }

        //使用するボールの名前を取得
        const ballName = this.titlescreen.returnBallname();
        switch(ballName) {
            case "Simple Ball":
                console.log("しんぷるぼーる");
                this._ballCreatePointer = 0;
                break;
            case "Coin Ball":
                console.log("こいんぼーる");
                this._ballCreatePointer = 1;
                break;
            case "Giant Ball":
                console.log("ぎがんとぼーる");
                this._ballCreatePointer = 2;
                break;
            case "Tiny Ball":
                console.log("たいにーぼーる");
                this._ballCreatePointer = 3;
                break;
        }

        this.objectSeting();
        
        this.availableBallsLabel.string = '×' + this._ballGenerateNum;

        this.rowCreate();
    },

    update: function (dt) {
        if (this._isGameOver == true)
        {
            this._particleProductionTimer -= dt;
            console.log(this._particleProductionTimer);
            if (this._particleProductionTimer < 0) {
                this.changeScreen();
            }
        }
    },

    objectSeting: function () {
        this.sampleBallSprite.spriteSeting(this._ballCreatePointer);
        switch (this._ballCreatePointer) {
            case 2:
            this.pointer.defaultPositionSet(0, -335);
            this.character.defaultPositionSet(0, -368);
            this.moveAvailableBallsLabel.setPosition(cc.v2(0, 60));
            break;

            case 3:
            this.pointer.defaultPositionSet(0, -410);
            this.character.defaultPositionSet(0, -410);
            this.moveAvailableBallsLabel.setPosition(cc.v2(0, 20));
            break;
        }
    },

    //RowPrefab複製
    rowCreate: function () {
        //配列の中身整理
        this.rowArrayElementDelete();
        //Row生成
        let parentNode = cc.find('Canvas/GameScreen/RowNode');
        let rowInstans = cc.instantiate(this.rowPrefab);
        rowInstans.setPosition(cc.v2(0,430));
        parentNode.addChild(rowInstans, 0, 0);
        this._rowArray[this._rowArrayPointer] = rowInstans;
        this._rowArrayPointer += 1|0;

        this._rowArrayLength = this._rowArray.length;
        this.schedule(this.rowMove, 0.01, 49, 0);
        this._isShoting = false;
    },

    //RowPrefabの移動
    rowMove: function () {
        let turn = 0;
        while (turn < this._rowArrayLength)
        {
            if (this._rowArray[turn] == null) return;

            this._rowArrayChildrenLength = this._rowArray[turn].childrenCount;

            let childTurn = 0;
            while (childTurn < this._rowArrayChildrenLength)
            {
                this._rowArray[turn].children[childTurn].setPositionY(this._rowArray[turn].children[childTurn].getPositionY() - 2);
                childTurn = (childTurn + 1)|0;
            }

            turn = (turn + 1)|0;
        }

        this._rowMoveCount = (this._rowMoveCount + 1)|0;
        if (this._rowMoveCount < 50 || this._isGameOver == true) return;
        this._isShotJudge = false;
    },

    //ObjectCreatorで呼び出し
    rowArrayElementDelete: function () {
        this._rowMoveCount = 0;
        for (let turn = 0; turn < this._rowArray.length; turn += 1|0)
        {
            if (this._rowArray[turn].children == null)
            {
                //要素削除
                this._rowArray.splice(turn, 1);
                this._rowArrayPointer -= 1|0;
                turn -= 1|0;
            }
        }
    },

    //ボール生成関数の呼び出し
    shootTheBall: function (angle) {
        //ボールに設定する角度を取得
        this._ballCreateConfigurationAngre = angle;
        //生成するボールの数を取得
        this._temporaryBallGenerateNum = this._ballGenerateNum;
        //0.1秒ごとにボールを生成
        this.schedule(this.ballCreate, 0.1, this._ballGenerateNum - 1, 0);
    },

    //ボール生成
    ballCreate: function () {
        this._temporaryBallGenerateNum -= 1|0;
        this.availableBallsLabel.string = '×' + this._temporaryBallGenerateNum;
        //ボールを生成する親ノードの場所を取得
        const parentNode = cc.find('Canvas/GameScreen/BallContainer');
        //ボール生成
        let ballInstans = cc.instantiate(this.ballPrefab[this._ballCreatePointer]);
        //ボール設置
        ballInstans.setPosition(cc.v2(this.pointerNode.getPositionX(), this.pointerNode.getPositionY()));
        ballInstans.rotation = this._ballCreateConfigurationAngre;
        //親ノードに追加する
        parentNode.addChild(ballInstans, 0, 0);

        if (this._temporaryBallGenerateNum <= 0)
        {
            this.character.switchingMainBallActive(false);
        }

        if (this._isShoting == true) return;
        this._isShoting = true;
        this._isShotJudge = true;
    },

    returnIsShoting: function () {
        return this._isShoting;
    },

    returnIsShotJudge: function () {
        return this._isShotJudge;
    },

    switchingIsShoting: function () {
        this.availableBallsLabel.string = '×' + this._ballGenerateNum;
        this.scorePlus();
        this.rowCreate();
        this.pointer.positionSet();
    },

    //スコア加算
    scorePlus: function () {
        this._score += 1|0;
        this.scoreLabel.string = this._score;
    },

    //現在のスコアを送る
    returnScore: function () {
        return this._score;
    },

    //コイン加算
    coinPlus: function () {
        this.dataManager.getCoin(1);
    },

    //ボール加算
    ballPlus: function () {
        this._ballGenerateNum += 1|0;
    },

    //HowTo閉じるボタン
    howToCloosButton: function () {
        console.log("CloosHowTo");
        this.howToNode.active = false;
    },

    //ポーズボタン
    pauseButton: function ()
    {
        console.log('PauseTest');
        cc.director.pause();
        this.pauseScreen.active = true;
    },

    //倍速ボタン
    speedUpButton: function () {
        if (this._isSpeedUp == false)
        {
            this._isSpeedUp = true;
            this.speedButtonLabel.string = "SPEEDDOWN";
            cc.director.getScheduler().setTimeScale(1.2);
        }
        else {
            this._isSpeedUp = false;
            this.speedButtonLabel.string = "SPEEDUP";
            cc.director.getScheduler().setTimeScale(1.0);
        }
    },

    returnIsSpeebUp: function () {
        return this._isSpeedUp;
    },

    //ゲームオーバー演出
    gameOverProduction: function () {
        console.log('gameover');
        this._isGameOver = true;
        this._isShotJudge = true;
        cc.audioEngine.playEffect(this.gameOverAudio, false);
        this.dataManager.scoreUpdata(this._score);
    },

    //ゲームオーバー画面に移動
    changeScreen: function () {
        console.log('GameOver');
        //ゲーム画面を非表示に
        this.gameScreen.active = false;
        //ゲームオーバー画面を表示する
        this.gameOverScreen.active = true;
    }
});
