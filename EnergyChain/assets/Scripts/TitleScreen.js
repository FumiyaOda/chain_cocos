//タイトル画面の制御全般

//変数宣言
//Datamanagerを使用
var DataManager = require('DataManager');

cc.Class({
    extends: cc.Component,

    properties: {
        //dataManager
        dataManager: {
            default: null,
            type: DataManager
        },
        //タイトル画面
        titleScreen: {
            default: null,
            type: cc.Node
        },
        //ショップ画面
        shopScreen: {
            default: null,
            type: cc.Node
        },
        //ゲーム画面
        gameScreen: {
            default: null,
            type: cc.Node
        },

        //インタースティシャル広告用変数
        _isInterstitial: 0,

        //動画広告用
        _preloadedRewardedVideo: null,

        //インタースティシャル広告用
        _interstitialLoad: null,

        //リワード広告ID
        adRewardedVideoID: "2330210970637844_2330212090637732",

        //インタースティシャル広告ID
        adInterstitialID: "2330210970637844_2330211970637744",

        //セットするボールの名前
        setingBall: "",
    },

    onLoad: function () {
        //乱数をセット(1～10)
        this._isInterstitial = Math.floor( Math.random () * 10) + 1;
        console.log(this._isInterstitial);

        //予め広告用の動画を読み込んでおく
        if (!cc.sys.os == 'mac')
        {
            //this.adInterstitialLoad();
            //this.adRewardedLoad();
        }
        this.adRewardedLoad();

        //4より小さいならインタースティシャル広告を読み込む
        if (this._isInterstitial < 5)
        {
            this.adInterstitialLoad();
        }

        //ユーザーデータから最後に選択しているボールの名前を取得
        this.getBallName(this.dataManager.returnSelectedBallName());
        //this.setingBall = this.dataManager.returnSelectedBallName();
    },

    //生成するボールの名前を取得
    getBallName: function (setBallName) {
        this.setingBall = setBallName;
    },

    returnBallname: function () {
        return this.setingBall;
    },

    //スタートボタン
    isStartButton: function () {
        console.log('GamePlay');
        //タイトル画面を非表示に
        this.titleScreen.active = false;
        //ゲーム画面を表示する
        this.gameScreen.active = true;
    },

    //ショップボタン
    isShopButton: function () {
        console.log('GoShop');
        //タイトル画面を非表示に
        this.titleScreen.active = false;
        //ショップ画面を表示する
        this.shopScreen.active = true;
    },

    //広告の読み込み
    adRewardedLoad: function () {
        console.log('リワード読み込み開始');
        const self = this;
        if (typeof FBInstant == 'undefined' || self._preloadedRewardedVideo != null) return;
        self._preloadedRewardedVideo = null;
        try {
            FBInstant.getRewardedVideoAsync(self.adRewardedVideoID).then(function(rewarded) {
                console.log('リワード読み込み');
                //非同期で広告をロード
                self._preloadedRewardedVideo = rewarded;
                console.log(self._preloadedRewardedVideo);
                return self._preloadedRewardedVideo.loadAsync();
            }).then(function() {
                console.log('動画を読み込み')
            }).catch(function(error){
                console.error('動画の読み込みに失敗しました：' + error.message);
            });
        }
        catch(error) {
            console.log(error);
        }
    },

    //インタースティシャル広告読み込み
    adInterstitialLoad: function () {
        console.log('インタースティシャル読み込み開始');
        const self = this;
        if (typeof FBInstant == 'undefined' || self._interstitialLoad != null) return;
        self._interstitialLoad = null;
        try {
            FBInstant.getInterstitialAdAsync(self.adInterstitialID).then(function(interstitial) {
                console.log('インタースティシャル読み込み');
                //非同期で広告をロード
                self._interstitialLoad = interstitial;
                console.log(self._interstitialLoad);
                return self._interstitialLoad.loadAsync();
            }).then(function () {
                console.log('広告を読み込み')
            }).catch(function (error) {
                console.error('広告の読み込みに失敗しました：' + error.message);
            }); 
        }
        catch(error) {
            console.log(error);
        }
    },

    //広告動画再生
    showRewardedAd: function() {
        console.log('動画再生');
        var self = this;
        if (self._preloadedRewardedVideo == null) return;
        //正常に動画を読み込んでいると再生する
        self._preloadedRewardedVideo.showAsync().then(function() {
            console.log('動画を再生');
            self._preloadedRewardedVideo = null;
        }).catch(function(e) {
            console.error(e.message);
        });
    },

    //インタースティシャル広告表示
    showInterstitialAd: function () {
        console.log('インタースティシャル広告表示');
        var self = this;
        if (self._interstitialLoad == null) return;
        //正常に広告を読み込んでいると表示
        self._interstitialLoad.showAsync().then(function () {
            console.log('広告表示');
            self._interstitialLoad = null;
        }).catch(function (error) {
            console.log(error.message);
        });
    }
});
