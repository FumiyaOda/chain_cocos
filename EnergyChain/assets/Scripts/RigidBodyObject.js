cc.Class({
    extends: cc.Component,

    properties: {
        rigibBodyNode: {
            default: null,
            type: cc.RigidBody
        }
    },

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        this.rigibBodyNode.enabledContactListener = true;
    },
});
