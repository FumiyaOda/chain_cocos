//変数宣言
var GameScreen = require('GameScreen');
var Character = require('Character');

cc.Class({
    extends: cc.Component,

    properties: {
        //GameScrren
        gameScreen: {
            default: null,
            type: GameScreen
        },
        //Character
        character: {
            default: null,
            type: Character
        }
    },

    update (dt) {
        //玉を発射している状態
        if (this.gameScreen.returnIsShoting() == true)
        {
            //自身の子がなくなった
            if (this.node.childrenCount <= 0)
            {
                //ボールのスタート位置を表示
                this.character.switchingMainBallActive(true);
                //発射状態解除
                this.gameScreen.switchingIsShoting();
            }        
        }
    },
});
