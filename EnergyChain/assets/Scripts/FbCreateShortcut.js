const DataManager = require('DataManager');

cc.Class({
    extends: cc.Component,

    properties: {
        dataManager: {
            default: null,
            type: DataManager
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.createShortcut();
    },

    createShortcut() {
        if (typeof FBInstant == 'undefined') return;
        const self = this;
        FBInstant.canCreateShortcutAsync().then(function (canCreateShortcut) {
            //一度でもショートカットが作成された場合作らない
            if (self.dataManager.returnIsCreateShortcutFlg() == true) return
                if (canCreateShortcut) {
                    FBInstant.createShortcutAsync().then(function () {
                            // Shortcut created
                            self.successCreateShortcut();
                        }).catch(function () {
                        // Shortcut not created
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
        }).catch(function (error) {
            console.log(error);
        });
    },

    //ショートカット作成成功時に呼ばれる
    successCreateShortcut() {
        //ショートカット作成リワード
        this.dataManager.createShortcutReward();
    }
});
