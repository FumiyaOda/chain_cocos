cc.Class({
    extends: cc.Component,

    properties: {
        plusBallLabel: cc.Prefab,
        plusCoinLabel: cc.Prefab,

        _labelInstans: null
    },

    // LIFE-CYCLE CALLBACKS:

    plusLabelCreator: function (labelNumber, setPositionX, setPositionY) {
        //数値で判断
        switch (labelNumber)
        {
            case 0:
            this._labelInstans = cc.instantiate(this.plusBallLabel);
            break;

            case 1:
            this._labelInstans = cc.instantiate(this.plusCoinLabel);
            break;
        }
        let parentNode = this.node;
        parentNode.addChild(this._labelInstans, 0, 0);
        this._labelInstans.setPosition(cc.v2(setPositionX, setPositionY));
    }
});
