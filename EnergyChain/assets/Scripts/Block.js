cc.Class({
    extends: cc.Component,

    properties: {
        blockLifeLabel: {
            default: null,
            type: cc.Label
        },

        _gameScreenNode: null,
        _gameScreen: null,
        _particleCreatorNode: null,
        _particleCreator: null,

        blockLife: 1,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._gameScreenNode = cc.find('Canvas/GameScreen');
        this._gameScreen = this._gameScreenNode.getComponent('GameScreen');
        this.blockLife = this._gameScreen.returnScore();
        this.blockLifeLabel.string = this.blockLife;

        this._particleCreatorNode = cc.find('Canvas/GameScreen/ParticleCreator');
        this._particleCreator = this._particleCreatorNode.getComponent('ParticleCreator');
    },

    onBeginContact: function (contact, self, other)
    {
        if (other.tag == 0)
        {
            this.blockLife -= 1;
            if (this.blockLife <= 0)
            {
                const blockPosX = this.node.getPositionX();
                const blockPosY = this.node.getPositionY();
                this._particleCreator.particleCreate(1, blockPosX, blockPosY + 410);
                this.node.destroy();
            }
            this.blockLifeLabel.string = this.blockLife;
        }

        if (other.tag == 7)
        {
            console.log("gameover");
            this._particleCreator.particleCreate(2, 0, -415);
            this._gameScreen.gameOverProduction();
        }
    }
});
