cc.Class({
    extends: cc.Component,

    properties: {
        sampleBallSprite: cc.Sprite,
        ballSprite: [cc.SpriteFrame]
    },

    // LIFE-CYCLE CALLBACKS:

    spriteSeting (num) {
        console.log(this.sampleBallSprite.spriteFrame);
        console.log(this.ballSprite[num]);
        this.sampleBallSprite.spriteFrame = this.ballSprite[num];

        switch (num) {
            case 2:
            this.node.scale = cc.v2(1,1);
            break;

            case 3:
            this.node.scale = cc.v2(0.05, 0.05);
            break;
        }
    }
});
