//ショップアイテムの商品と価格、選択とロック状態の管理


cc.Class({
    extends: cc.Component,

    properties: {
        //ボール
        ball: {
            default: null,
            type: cc.Node
        },

        //商品価格
        itemPrice: 0,

        //ロック解除状態
        isUnlocked: false,

        //予めロックが解除されているか
        isUnlockedByDefault: false,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        const dataManagerNode = cc.find('Canvas/DataManager');
        console.log(dataManagerNode);
        const dataManager = dataManagerNode.getComponent('DataManager');
        this.isUnlockedByDefault = dataManager.returnBallUnLock(this.node.name);
        console.log(dataManager.returnBallUnLock(this.node.name));
        console.log(this.isUnlockedByDefault);

        if(this.isUnlockedByDefault)
        {
            this.isUnlocked = true;
        }
        else {
            this.isUnlocked = false;
        }
    },

    update: function (dt) {
        //ロック解除の状態で色を変更
        if(this.isUnlocked)
        {
            this.ball.color = new cc.Color(255, 255, 255, 255);
        }
        else {
            this.ball.color = new cc.Color(0, 0, 0, 255);
        }
    },

    switchingisUnlockedByDefault: function () {
        this.isUnlockedByDefault = true;
    }
});
