cc.Class({
    extends: cc.Component,

    properties: {
        particlePrefab: cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        const myParticle = this.particlePrefab.getComponent(cc.ParticleSystem);
        myParticle.resetSystem();
    },
});
