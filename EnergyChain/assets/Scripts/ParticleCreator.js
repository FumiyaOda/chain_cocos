cc.Class({
    extends: cc.Component,

    properties: {
        blockHitParticle: cc.Prefab,
        blockBreakParticle: cc.Prefab,
        gameOverParticle: cc.Prefab,
        wallHitParticle: cc.Prefab,

        _particleInstans: null
    },

    // LIFE-CYCLE CALLBACKS:

    particleCreate: function (particleNumber, setPositionX, setPositionY) {
        //数値で判断
        switch (particleNumber)
        {
            case 0:
            this._particleInstans = cc.instantiate(this.blockHitParticle);
            break;

            case 1:
            this._particleInstans = cc.instantiate(this.blockBreakParticle);
            break;

            case 2:
            this._particleInstans = cc.instantiate(this.gameOverParticle);
            break;

            case 3:
            this._particleInstans = cc.instantiate(this.wallHitParticle);
            break;
        }
        let parentNode = this.node;
        parentNode.addChild(this._particleInstans, 0, 0);
        this._particleInstans.setPosition(cc.v2(setPositionX, setPositionY));
    }
});
