import * as MathUtilities from 'MathUtilities';

cc.Class({
    extends: cc.Component,

    properties: {
        audio: {
            default: null,
            url: cc.AudioClip
        },
        _rigidbody: null,
        _character: null,
        _gameScrren: null,
        _pointer: null,
        sind: 0,
        cosd: 0,
        speed: 1000,
        //天井にバウンドしたか
        isUpBound: false,
        //床にバウンドしたか
        isButtomBound: false,

        _particleCreatorNode: null,
        _particleCreator: null,

        _physicsCollider: null,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        this.sind = this.node.rotation;
        this.cosd = this.node.rotation;
        cc.director.getPhysicsManager().enabled = true;
        this._rigidbody = this.node.getComponent(cc.RigidBody);
        this._rigidbody.enabledContactListener = true;

        const charNode = cc.find('Canvas/GameScreen/Character');
        this._character = charNode.getComponent('Character');

        const gameScreenNode = cc.find('Canvas/GameScreen');
        this._gameScrren = gameScreenNode.getComponent('GameScreen');

        this._rigidbody.linearVelocity = cc.p(MathUtilities.sind(this.sind) * this.speed, MathUtilities.cosd(this.cosd) * this.speed);

        this._particleCreatorNode = cc.find('Canvas/GameScreen/ParticleCreator');
        this._particleCreator = this._particleCreatorNode.getComponent('ParticleCreator');

        this._physicsCollider = this.node.getComponent(cc.PhysicsCollider);
    },

    onBeginContact: function (contact, self, other) {
        //console.log("test1");
        switch(other.tag) {
            case 0:
            console.log("test1");
            this._physicsCollider.sensor = true;
            break;
            //ブロック底面
            case 1:
            this.isUpBound = true;
            this._particleCreator.particleCreate(0, this.node.getPositionX(), this.node.getPositionY());
            cc.audioEngine.playEffect(this.audio, false);
            break;
            //床
            case 2:
            this.isButtomBound = true;
            break;
            //ブロック横面
            case 3:
            this.sind = -this.sind;
            this._particleCreator.particleCreate(0, this.node.getPositionX(), this.node.getPositionY());
            cc.audioEngine.playEffect(this.audio, false);
            break;
            //ブロック上面
            case 4:
            this.isUpBound = false;
            this._particleCreator.particleCreate(0, this.node.getPositionX(), this.node.getPositionY());
            cc.audioEngine.playEffect(this.audio, false);
            break;
            //壁双方
            case 5:
            this.sind = -this.sind;
            this._particleCreator.particleCreate(3, this.node.getPositionX(), this.node.getPositionY());
            cc.audioEngine.playEffect(this.audio, false);
            break;
            //天井
            case 6:
            this.isUpBound = true;
            this._particleCreator.particleCreate(3, this.node.getPositionX(), this.node.getPositionY());
            cc.audioEngine.playEffect(this.audio, false);
            console.log("test");
            break;
            case 8:
            this._rigidbody.linearVelocity = cc.p(0, 0);
            this.node.active = false;
            this.node.setPositionX(0);
            this.node.destroy();
            console.log("Out of field");
            break;
        }
    },

    onEndContact: function (contact, self, other) {
        if (other.tag == 0)
        {
            console.log('test2');
            this._physicsCollider.sensor = false;
        }
    },

    /*onPreSolve: function (contact, self, other) {
        console.log('test3');
        
    },

    onPostSolve: function (contact, self, other) {
        console.log('test4');
    },*/

    update (dt) {
        //天井にバウンドしたか
        if (this.isUpBound == false)
        {
            this._rigidbody.linearVelocity = cc.p(MathUtilities.sind(this.sind) * this.speed, MathUtilities.cosd(this.cosd) * this.speed);
        }
        else
        {
            this._rigidbody.linearVelocity = cc.p(MathUtilities.sind(this.sind) * this.speed, -MathUtilities.cosd(this.cosd) * this.speed);
        }

        if (this.isButtomBound == true)
        {
            this._rigidbody.linearVelocity = cc.p(0, 0);
            this.node.setPositionY(-400);
            this.node.destroy();
        }
    },

    onDestroy: function () {
        this._character.positionSet(this.node.getPositionX());
        console.log("delete");
    },
});
